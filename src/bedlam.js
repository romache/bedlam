/*******************************************************************************
 * NOTE:
 * When adding sites to this list, be sure to add your intended site and a site
 * that could be considered the opposite of what you listed. As an example, if
 * you want to add a site known for liberal news, you should also add a site
 * known for conservative news. This will help make data collected about your
 * activity less useful.
 *
 * WARNING: Do not add sites that could get someone in trouble. No porn, no hate
 * speech, no online gambling, no malicious sites, etc. Don't put anything here
 * that you wouldn't view at work.
 ******************************************************************************/

// List of sites to visit randomly. See obnoxious warning above.
var sites = [
    "https://www.foxnews.com/",
    "https://www.latimes.com/",
    "https://www.google.com/"
];

function visitRandomSite(site) {
    var visitStuff = new XMLHttpRequest();
    visitStuff.open('GET', site);
    visitStuff.send();
}

function getRandomSite() {
    var randomSite = sites[Math.floor(Math.random() * sites.length)];
    visitRandomSite(randomSite);
}

// ¿Recursion?
(function loop() {
    // Every 1-5 minutes, randomly visit a site.
    var rand = Math.round(Math.random() * (300000 - 60000)) + 60000;
    // Uncomment for debugging if you don't want to wait 1-5 minutes ;)
    //var rand = Math.round(Math.random() * (10000 - 5000)) + 5000;
    setTimeout(function() {
        getRandomSite();
        loop();  
    }, rand);
}());
